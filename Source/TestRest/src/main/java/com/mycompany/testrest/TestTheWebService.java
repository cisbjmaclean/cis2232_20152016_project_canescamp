/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.testrest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import javax.swing.JOptionPane;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author bjmaclean
 */
public class TestTheWebService {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {

            URL url = new URL("http://localhost:8080/canes/rest/address/6");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            String theOutput = "";
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                theOutput += output;
                System.out.println(output);
            }

            conn.disconnect();
            
            ObjectMapper om = new ObjectMapper();
            Address address = om.readValue(theOutput, Address.class);
            
            JOptionPane.showMessageDialog(null, theOutput);
            JOptionPane.showMessageDialog(null, address.toString());
        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
    }
}
