/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.service;

import info.hccis.admin.model.Camp;
import java.util.List;

/**
 *
 * @author krystofurr
 */
public interface CampService {
    
    public abstract List<Camp> getCamps();
    
    public abstract Camp getCamp(int id);
    
}
