/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.service;

import info.hccis.admin.model.RegistrationSimple;
import java.util.List;

/**
 *
 * @author krystofurr
 */
public interface RegistrationService {
    
    public abstract List<RegistrationSimple> getRegistrations();
    
    public abstract RegistrationSimple getRegistration(int id);
    
}
