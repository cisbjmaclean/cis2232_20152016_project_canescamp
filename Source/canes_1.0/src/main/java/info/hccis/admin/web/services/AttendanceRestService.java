package info.hccis.admin.web.services;

import info.hccis.admin.model.Attendance;
import info.hccis.admin.service.AttendanceService;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;
 
@Component
@Path("/attendance")
@Scope("request")
@Produces("application/json")
public class AttendanceRestService {
    
    @Resource
    private final AttendanceService attendanceService;
    
    private final static Logger LOGGER = Logger.getLogger(ActivityRestService.class.getName());
    
    public AttendanceRestService(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        
        this.attendanceService = applicationContext.getBean(AttendanceService.class);
    }
    
    @GET
    @Path("/getAll")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Attendance> getAttendances() {
        System.out.println("[ REST SERVICE - Attendance ]: Getting all attendances");
        
        List<Attendance> attendances = attendanceService.getAttendances();
        if (attendances == null) {
            LOGGER.severe("No attendances exist");
        } else {
            return attendances;
        }
        
        return null;
    }
    
    @GET
    @Path("/get/{param}")
    @Produces(MediaType.APPLICATION_JSON)
    public Attendance getAttendance(@PathParam("param") int id) {
        System.out.println("[ REST SERVICE - Attendance ]: Getting a single attendance with ID: " + id);
        
        Attendance attendance = attendanceService.getAttendance(id);
        if (attendance == null) {
            LOGGER.log(Level.SEVERE, "No attendance with ID={0} exists", id);
        } else {
            return attendance;
        }
        
        return null;
    }
    
    @POST
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response saveAttendance(Attendance attendance) {
        System.out.println("[ REST SERVICE - Attenance ]: Saving Attendance...");
        
        attendanceService.saveAttendance(attendance);
        
        return Response.status(200).entity("[ REST SERVICE - Attendance ]: Save successful").build();
    }
    
    @POST
    @Path("/saveAll")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response saveAttendances(List<Attendance> attendances) {
        System.out.println("[ REST SERVICE - Attenance ]: Saving Attendances...");
        
        for (Attendance attendance : attendances) {
            System.out.println("Save => " + attendance.toString());
        }
        
        attendanceService.saveAttendances(attendances);
        return Response.status(200).entity("[ REST SERVICE - Attendance ]: Save all successful").build();
    }
}
