/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author BJ
 */
@Entity
@Table(name = "Registration")
public class Registration implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "registrationId")
    private Integer registrationId;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "name")
    private String name;
    
    @Size(max = 10)
    @Column(name = "dob")
    private String dob;
    
    @Column(name = "gradeCode")
    private Integer gradeCode;
    
    @Column(name = "genderCode")
    private Integer genderCode;
    
    @Size(max = 50)
    @Column(name = "school")
    private String school;

    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 50)
    @Column(name = "email")
    private String email;
    
    @Size(max = 250)
    @Column(name = "otherDescription")
    private String otherDescription;
    
    @Column(name = "permissionLeaveInd")
    private boolean permissionLeaveInd;
    
    @Column(name = "permissionPhoto")
    private boolean permissionPhoto;
    
    @Column(name = "permissionWaiverInd")
    private boolean permissionWaiverInd;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "paymentAmount")
    private Double paymentAmount;
    
    @JoinColumn(name = "contactId1")
    @NotNull
    @OneToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER, optional = false)
    @MapsId
    @Valid
    private Contact contactId1;

    @JoinColumn(name = "contactId2")
    @NotNull
    @OneToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER, optional = false)
    @MapsId
    @Valid
    private Contact contactId2;

        @JoinColumn(name = "contactId3")
    @NotNull
    @OneToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER, optional = false)
    @MapsId
    @Valid
    private Contact contactId3;

        @NotNull
    private int campId;

    @NotNull
    @OneToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER, optional = false)
    @MapsId
    @Valid
    //http://forum.spring.io/forum/spring-projects/web/75887-jsr303-validation-for-nested-objects
    @JoinColumn(name = "addressId")
    private Address addressId;

    public Registration() {
    }

    public Registration(Integer registrationId) {
        this.registrationId = registrationId;
    }

    public Registration(Integer registrationId, String name) {
        this.registrationId = registrationId;
        this.name = name;
    }

    public Integer getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(Integer registrationId) {
        this.registrationId = registrationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public Integer getGradeCode() {
        return gradeCode;
    }

    public void setGradeCode(Integer gradeCode) {
        this.gradeCode = gradeCode;
    }

    public Integer getGenderCode() {
        return genderCode;
    }

    public void setGenderCode(Integer genderCode) {
        this.genderCode = genderCode;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOtherDescription() {
        return otherDescription;
    }

    public void setOtherDescription(String otherDescription) {
        this.otherDescription = otherDescription;
    }

    public boolean getPermissionLeaveInd() {
        return permissionLeaveInd;
    }

    public void setPermissionLeaveInd(boolean permissionLeaveInd) {
        this.permissionLeaveInd = permissionLeaveInd;
    }

    public boolean getPermissionPhoto() {
        return permissionPhoto;
    }

    public void setPermissionPhoto(boolean permissionPhoto) {
        this.permissionPhoto = permissionPhoto;
    }

    public boolean getPermissionWaiverInd() {
        return permissionWaiverInd;
    }

    public void setPermissionWaiverInd(boolean permissionWaiverInd) {
        this.permissionWaiverInd = permissionWaiverInd;
    }

    public Double getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(Double paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

//  s
    public Address getAddressId() {
        return addressId;
    }

    public void setAddressId(Address addressId) {
        this.addressId = addressId;
    }

    public Contact getContactId1() {
        return contactId1;
    }

    public void setContactId1(Contact contactId1) {
        this.contactId1 = contactId1;
    }

    public Contact getContactId2() {
        return contactId2;
    }

    public void setContactId2(Contact contactId2) {
        this.contactId2 = contactId2;
    }

    public Contact getContactId3() {
        return contactId3;
    }

    public void setContactId3(Contact contactId3) {
        this.contactId3 = contactId3;
    }

    public int getCampId() {
        return campId;
    }

    public void setCampId(int campId) {
        this.campId = campId;
    }

    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (registrationId != null ? registrationId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Registration)) {
            return false;
        }
        Registration other = (Registration) object;
        if ((this.registrationId == null && other.registrationId != null) || (this.registrationId != null && !this.registrationId.equals(other.registrationId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.hccis.admin.model.Registration[ registrationId=" + registrationId + " ]";
    }

}
