package info.hccis.canes.dao.repository;

import info.hccis.admin.model.Camp;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CampRepository extends CrudRepository<Camp, Integer> {
    
    

}