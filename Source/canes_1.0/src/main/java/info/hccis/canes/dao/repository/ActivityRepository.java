/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.canes.dao.repository;

import info.hccis.admin.model.Activity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * JPQL
 * REF: http://www.tutorialspoint.com/jpa/jpa_jpql.htm
 * @author krystofurr
 */
@Repository
public interface ActivityRepository extends CrudRepository<Activity, Integer>{
    
    /**
     * Will update an activity with automated timestamp
     * 
     * 
     * @param name
     * @param description
     * @param rules
     * @param comments
     * @param rating
     * @param activityId
     * @param campID
     * @return 
     */

    @Transactional
    @Modifying
    @Query("UPDATE Activity AS a SET a.name = :name, "
            + "a.description = :description, "
            + "a.rules = :rules, "
            + "a.comments = :comments, "
            + "a.rating = :rating, "
            + "a.syncstamp = NOW() "
            + "WHERE a.activityId = :activityId")
    public abstract void updateActivityWithSyncStamp(@Param("activityId") int activityId, @Param("name") String name, @Param("description") String description, 
                                        @Param("rules") String rules, @Param("comments") String comments, @Param("rating") int rating );
    
    /**
     * Updates an activity with manual timestamp
     * 
     * @param activityId
     * @param name
     * @param description
     * @param rules
     * @param comments
     * @param rating
     * @param syncStamp 
     */
    @Transactional
    @Modifying
    @Query("UPDATE Activity AS a SET a.name = :name, "
            + "a.description = :description, "
            + "a.rules = :rules, "
            + "a.comments = :comments, "
            + "a.rating = :rating, "
            + "a.syncstamp = :syncstamp "
            + "WHERE a.activityId = :activityId")
    public abstract void updateActivity(@Param("activityId") int activityId, @Param("name") String name, @Param("description") String description, 
                                        @Param("rules") String rules, @Param("comments") String comments, @Param("rating") int rating,
                                        @Param("syncstamp") String syncStamp);
    

}
