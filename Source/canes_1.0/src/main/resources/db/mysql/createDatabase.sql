-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 11, 2015 at 04:50 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `canes`
--

create database cis2232_canes;
use cis2232_canes;
--
-- Table structure for table `address`
--

CREATE TABLE IF NOT EXISTS `Address` (
  `addressId` int(5) NOT NULL AUTO_INCREMENT COMMENT 'PK ',
  `mailingAddress` varchar(100) NOT NULL COMMENT 'Street address',
  `city` varchar(50) NOT NULL COMMENT 'City ',
  `postalCode` varchar(6) DEFAULT NULL COMMENT 'Postal code',
  PRIMARY KEY (`addressId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `address`
--

INSERT INTO `Address` (`addressId`, `mailingAddress`, `city`, `postalCode`) VALUES
(1, '5', '6', '7'),
(3, '1', '2', '2');

-- --------------------------------------------------------

--
-- Table structure for table `camp`
--

CREATE TABLE IF NOT EXISTS `Camp` (
  `campId` int(5) NOT NULL AUTO_INCREMENT COMMENT 'This is pk',
  `description` varchar(50) NOT NULL COMMENT 'Name of the camp',
  `descriptionLong` varchar(250) NOT NULL COMMENT 'Long description ',
  `startDate` varchar(10) NOT NULL COMMENT 'Start date',
  `endDate` varchar(10) NOT NULL COMMENT 'End date',
  `capacity` int(3) NOT NULL COMMENT 'Number of campers',
  `contactName` varchar(50) NOT NULL COMMENT 'Contact person',
  PRIMARY KEY (`campId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `code_type`
--
--
-- Table structure for table `contact`
--

CREATE TABLE IF NOT EXISTS `Contact` (
  `contactId` int(5) NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `name` varchar(50) NOT NULL COMMENT 'Name of contact',
  `phoneHome` varchar(10) DEFAULT NULL COMMENT 'Home phone',
  `phoneCell` varchar(10) DEFAULT NULL COMMENT 'Cell phone',
  `phoneWork` varchar(10) DEFAULT NULL COMMENT 'Work phone',
  `mailingAddress` varchar(200) DEFAULT NULL COMMENT 'Mailing address',
  PRIMARY KEY (`contactId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `registration`
--

CREATE TABLE IF NOT EXISTS `Registration` (
  `registrationId` int(5) NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `name` varchar(50) NOT NULL COMMENT 'Kids name',
  `dob` varchar(12) DEFAULT NULL COMMENT 'Birthday string (yyyymmdd)',
  `gradeCode` int(3) DEFAULT NULL COMMENT 'Code type 11',
  `genderCode` int(3) DEFAULT NULL COMMENT 'Code type 2',
  `school` varchar(50) DEFAULT NULL COMMENT 'School name',
  `addressId` int(5) DEFAULT NULL COMMENT 'FK to address table',
  `email` varchar(50) DEFAULT NULL COMMENT 'email address',
  `otherDescription` varchar(250) DEFAULT NULL,
  `campId` int(5) DEFAULT NULL COMMENT 'FK to camp table',
  `contactId1` int(5) DEFAULT NULL COMMENT 'FK to contact for contact 1',
  `contactId2` int(5) DEFAULT NULL COMMENT 'FK to contact for contacct 2',
  `contactId3` int(5) DEFAULT NULL COMMENT 'FK to contact for other contacct',
  `permissionLeaveInd` int(1) DEFAULT NULL COMMENT 'Indicator or permission to leave premiises after camp',
  `permissionPhoto` int(1) DEFAULT NULL COMMENT 'Indicator for photos',
  `permissionWaiverInd` int(1) DEFAULT NULL COMMENT 'Indicator that waiver is given',
  `paymentAmount` double(6,2) DEFAULT NULL COMMENT 'Amount paid',
  PRIMARY KEY (`registrationId`),
  KEY `contactId1` (`contactId1`),
  KEY `contactId2` (`contactId2`),
  KEY `contactId3` (`contactId3`),
  KEY `campId` (`campId`),
  KEY `addressId` (`addressId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Main table containing registration information' AUTO_INCREMENT=4 ;

--
-- Dumping data for table `registration`
--

INSERT INTO `Registration` (`registrationId`, `name`, `dob`, `gradeCode`, `genderCode`, `school`, `addressId`, `email`, `otherDescription`, `campId`, `contactId1`, `contactId2`, `contactId3`, `permissionLeaveInd`, `permissionPhoto`, `permissionWaiverInd`, `paymentAmount`) VALUES
(1, '1', '2', 3, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, '1', '2', 3, 1, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `registration`
--
ALTER TABLE `Registration`
  ADD CONSTRAINT `registration_ibfk_1` FOREIGN KEY (`contactId1`) REFERENCES `Contact` (`contactId`),
  ADD CONSTRAINT `registration_ibfk_2` FOREIGN KEY (`contactId2`) REFERENCES `Contact` (`contactId`),
  ADD CONSTRAINT `registration_ibfk_3` FOREIGN KEY (`contactId3`) REFERENCES `Contact` (`contactId`),
  ADD CONSTRAINT `registration_ibfk_4` FOREIGN KEY (`campId`) REFERENCES `Camp` (`campId`),
  ADD CONSTRAINT `registration_ibfk_5` FOREIGN KEY (`addressId`) REFERENCES `Address` (`addressId`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

CREATE TABLE IF NOT EXISTS `CodeType` (
  `codeTypeId` int(3) NOT NULL COMMENT 'This is the primary key for code types',
  `englishDescription` varchar(100) NOT NULL COMMENT 'English description',
  `frenchDescription` varchar(100) DEFAULT NULL COMMENT 'French description',
  `createdDateTime` datetime DEFAULT NULL,
  `createdUserId` varchar(20) DEFAULT NULL,
  `updatedDateTime` datetime DEFAULT NULL,
  `updatedUserId` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1 COMMENT='This hold the code types that are available for the applicat';

--
-- Dumping data for table `code_type`
--



CREATE TABLE IF NOT EXISTS `CodeValue` (
  `codeTypeId` int(3) NOT NULL COMMENT 'see code_type table',
  `codeValueSequence` int(3) NOT NULL,
  `englishDescription` varchar(100) NOT NULL COMMENT 'English description',
  `englishDescriptionShort` varchar(20) NOT NULL COMMENT 'English abbreviation for description',
  `frenchDescription` varchar(100) DEFAULT NULL COMMENT 'French description',
  `frenchDescriptionShort` varchar(20) DEFAULT NULL COMMENT 'French abbreviation for description',
  `createdDateTime` datetime DEFAULT NULL,
  `createdUserId` varchar(20) DEFAULT NULL,
  `updatedDateTime` datetime DEFAULT NULL,
  `updatedUserId` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COMMENT='This will hold code values for the application.';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `code_type`
--
ALTER TABLE `CodeType`
  ADD PRIMARY KEY (`codeTypeId`);

--
-- Indexes for table `code_value`
--
ALTER TABLE `CodeValue`
  ADD PRIMARY KEY (`codeValueSequence`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `code_type`
--
ALTER TABLE `CodeType`
  MODIFY `CodeTypeId` int(3) NOT NULL AUTO_INCREMENT COMMENT 'This is the primary key for code types',AUTO_INCREMENT=1;

ALTER TABLE `CodeValue`
  MODIFY `codeValueSequence` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

ALTER TABLE CodeValue
    ADD FOREIGN KEY (codeTypeId)
    REFERENCES CodeType(codeTypeId);

--
-- Table structure for table `useraccess`
--

CREATE TABLE IF NOT EXISTS `UserAccess` (
  `userAccessId` int(3) NOT NULL,
  `username` varchar(100) NOT NULL COMMENT 'Unique user name for app',
  `password` varchar(128) NOT NULL,
  `userTypeCode` int(3) NOT NULL DEFAULT '1' COMMENT 'Code type #1',
  `createdDateTime` datetime DEFAULT NULL COMMENT 'When user was created.'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE UserAccess
ADD FOREIGN KEY (userTypeCode)
REFERENCES CodeValue(codeValueSequence);

--
-- Indexes for table `useraccess`
--
ALTER TABLE UserAccess
  ADD PRIMARY KEY (`userAccessId`);

--
-- AUTO_INCREMENT for table `useraccess`
--

ALTER TABLE `UserAccess`
  MODIFY `userAccessId` int(3) NOT NULL AUTO_INCREMENT;


--
-- Dumping data for table `codevalue`
--

INSERT INTO `CodeType` (`CodeTypeId`, `englishDescription`, `frenchDescription`, `createdDateTime`, `createdUserId`, `updatedDateTime`, `updatedUserId`) VALUES
(1, 'User Types', 'User Types FR', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(11, 'Gender Types', 'User Types FR', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(12, 'Grades', 'GradesFR', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '');


INSERT INTO `CodeValue` (`codeTypeId`, `codeValueSequence`, `englishDescription`, `englishDescriptionShort`, `frenchDescription`, `frenchDescriptionShort`, `createdDateTime`, `createdUserId`, `updatedDateTime`, `updatedUserId`) VALUES
(1, 1, 'General', 'General', 'GeneralFR', 'GeneralFR', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(11, 2, 'Boy', 'B', 'GeneralFR', 'GeneralFR', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(11, 3, 'Girl', 'G', 'GeneralFR', 'GeneralFR', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(12, 4, 'Kindergarden', 'K', '', '', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(12, 5, '1', '1', '', '', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(12, 6, '2', '2', '', '', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(12, 7, '3', '3', '', '', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(12, 8, '4', '4', '', '', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(12, 9, '5', '5', '', '', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(12, 10, '6', '6', '', '', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(12, 11, '7', '7', '', '', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(12, 12, '8', '8', '', '', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(12, 13, '9', '9', '', '', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(12, 14, '10', '10', '', '', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(12, 15, '11', '11', '', '', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(12, 16, '12', '12', '', '', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin');

/*create a user in database*/
/*create a user in database*/
grant select, insert, update, delete on cis2232_canes.*
             to 'cis2232_admin'@'localhost'
             identified by 'Test1234';
flush privileges;